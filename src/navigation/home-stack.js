import { createStackNavigator } from 'react-navigation-stack';
// import home from '../screens/home/home';
import HomeTabs from '../navigation/home-tab';

import { homeStack } from '../config/navigator';

export default createStackNavigator(
    {
        // home: home,
        [homeStack.home]: HomeTabs,

    },
    {
        defaultNavigationOptions: {
            header: null,
        },
    }
);