import Colors from "./Colors";
import { screen } from '@/utils';

export default {
    item: {
        position: 'relative',
        paddingVertical: 10,
        paddingRight: 5,
        width: '90%',
    },
    iconItem: {
        position: 'relative',
        paddingVertical: 10,
        paddingRight: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        borderTopColor: '#ccc',
        paddingLeft: 10,
        width: '98%',
        zIndex: 1,
        marginBottom: 8,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    btn_top: {
        width: 40,
        height: 40,
        position: 'absolute',
        right: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
    },
    btn_bottom: {
        width: 40,
        height: 40,
        position: 'absolute',
        bottom: 10,
        top: 0,
        right: -20,
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor:Colors.mainColor,
        // borderWidth:0.6,
        borderRadius: 20,
        backgroundColor: '#fff',
        shadowColor: Colors.mainColor,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 8,
    },
    actionButtonIcon: {
        color: 'white',
    },
    component: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 10,
    },
    row: {
        flexDirection: 'row',
    },
    formGroupL: {
        position: 'relative',
        flex: 1,
        right: 2
    },
    formGroupR: {
        position: 'relative',
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.bgContainerColor,
    },
    imageBackground: {
        position: 'absolute',
        width: screen.widthsc,
        height: screen.heightsc,
    },
    textDancingScript: {
        fontFamily: 'DancingScript-Bold',
    },
    textSariaSemiCondensed: {
    },
    footer: {
        bottom: (screen.heightsc / 4) * -1,
        left: 0,
        right: 0,
        alignItems: 'center',
        position: 'relative',
    },
    textfooter: {
        color: Colors.greyColor,
    },
    flashMessageStyle: {
    }
};