import { createStackNavigator } from 'react-navigation-stack';

import SearchScreen from '../screens/search/search.screen';

import { searchStack } from '../config/navigator';

export default createStackNavigator(
    {
        // home: home,
        [searchStack.search]: SearchScreen,

    },
    {
        defaultNavigationOptions: {
            header: null,
        },
    }
);