import commonStyles from './commonStyles';
import  Colors, { colorOpacityMaker, percentToHex }  from './Colors';
import { sizeFont, sizeScale, sizeHeight, sizeWidth } from './Responsive';
import Spanding from './Spanding';
import Constants from './Constants';
import Fonts from './Fonts';

export {
    Colors,
    commonStyles,
    Spanding,
    Constants,
    sizeFont, 
    sizeScale, 
    sizeHeight, 
    sizeWidth,
    colorOpacityMaker, 
    percentToHex,
    Fonts
}
