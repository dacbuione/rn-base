import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Image, Dimensions } from 'react-native'
import { Colors } from '@/commons'
import DatePicker from 'react-native-datepicker'
import Icon from 'react-native-vector-icons/FontAwesome'



export default class InputDateTime extends Component {
    render() {
        const {
            value,
            onDateChange,
            format,
            placeholderTextColor,
            placeholder,
            img,
            width
          } = this.props;
        const widthIcon =  Dimensions.get('window').width* -(0.58);
        return (
            <View style={[styles.rowInput,{width:width?width:null}]}>
                { value != '' ? 
                <View style={styles.viewLable}>
                    <Text style={styles.textLable}>{placeholder}</Text>
                </View> : null }
                <DatePicker
                    date={value}
                    mode="date"
                    placeholder={placeholder}
                    format={format}
                    confirmBtnText="Ok"
                    cancelBtnText="Hủy"
                    iconComponent={
                    <Icon  
                      name="" 
                      size={16} 
                      color="#999" 
                      style={{
                        position: 'absolute',
                        top: 10,
                      }} 
                      />
                    }
                    customStyles={{
                      dateInput: {
                        borderWidth: 0,
                        alignItems: "flex-start",
                        paddingLeft:15,
                      },
                    }}
                    onDateChange={onDateChange}
                    
                  />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    viewLable: {
        fontSize:10,
        backgroundColor:'#fff',
        width:100, 
        marginTop:-8, 
        marginLeft:10,
        position:'absolute'
    },
    textLable:{
        fontSize:10,
        left:10
    },
   
    rowInput:{
        borderColor:Colors.mainColor, 
        borderWidth:1,
        // borderRadius:10, 
        height:40,
        flexDirection:'row',
        marginTop:11,
        position:'relative',
    },
    iconView:{
        position:'absolute',
        right:5,
        top:10
    },
    icon:{
        width: 18,
        height:18,
    },
    TextInput:{
        height: 40,
        paddingVertical: 5,
        paddingLeft:15,
        width:'100%'
    }
})
