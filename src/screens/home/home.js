import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import { go_muahang } from '../../modules/home/action';


import screen from '../../utils/screen';
import Widgets from '../../components/widgets/index';

class home extends Component {
    logout = () => {
        this.props.dispatch(signOut())
    }

    muahang = () => {
        this.props.dispatch(go_muahang())
        // this.props.dispatch(go_popup_view())
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>HomeScreen</Text>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    header: {
        width: screen.widthsc,
        height: screen.heightsc / 12,
        borderBottomWidth: 1,
        borderBottomColor: '#EEEEEE',
        flexDirection: 'row',
    },
    userHeader: {
        position: 'absolute',
        width: screen.heightsc / 20,
        height: screen.heightsc / 20,
        marginLeft: screen.heightsc / 100,
        marginTop: screen.heightsc / 60,
    },
    userName: {
        marginLeft: screen.heightsc / 14,
        marginTop: screen.heightsc / 30,
    },
    widgetsview: {
        marginTop: 30,
        marginLeft: 30,
        marginRight: 30,
        flexDirection: 'row',
    },
})

export default connect(mapStateToProps, null)(home)