import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import { mainStack } from '../config/navigator';


import HomeStack from './home-stack';


export default createStackNavigator(
    {
        [mainStack.home]: HomeStack,
    },
    {
        defaultNavigationOptions: {
            header: null,
          },
    }
);
