import * as types from './constants';
// import appState from '../contants/initialState';

const login = ({
  userInfo: {},
})

const loginReducers = (state = login, action) => {
  switch (action.type) {
    case types.SET_USER_INFO:
      return { ...state, ...{ userInfo: action.data } }
    default:
      return state
  }
}

export default loginReducers