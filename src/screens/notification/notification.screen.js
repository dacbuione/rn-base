import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import HeaderList from '../../components/header/headerdanhsach';

import { data_screen } from '../../mock/notifi.screen.mock';

class NotificationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: data_screen,
            isRefreshing: false,
            isScroll: true,
            count: 0,
            page: 2,
        };
    }

    componentDidMount = () => {
        console.log('data.mock', this.state.data)
    }

    refresh = async () => {

    }

    renderItems = ({ item }) => {
        <TouchableOpacity>
            {console.log('item', item)}
            <Text>{item.stt}</Text>
        </TouchableOpacity>
    }

    onLoadMore = () => {

    }

    render() {
        return (
            <View style={styles.container}>
               <Text>NotificationScreen</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    }
})

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, null)(NotificationScreen)