import React, { Component } from 'react';
import { Provider } from 'react-redux';
// import redux from './config/redux';
// import { AppContainer } from './navigation/root-switch';
import AppRouter from './app_router';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './config-store';

const { store, persistor } = configureStore();

const App = () => {
  console.disableYellowBox = true;
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppRouter />
      </PersistGate>
    </Provider>
  )
}
export default App