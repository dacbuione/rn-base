import { Dimensions } from 'react-native'

export default screen =  ({
    widthsc: Dimensions.get('screen').width,
    heightsc: Dimensions.get('screen').height
})