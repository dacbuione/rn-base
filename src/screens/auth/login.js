import React, { PureComponent } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    View,
    StyleSheet,
    Text,
    Button,
    Dimensions,
    Platform,
    KeyboardAvoidingView,
    TouchableOpacity,
    Image,

} from 'react-native';
import { connect } from 'react-redux';
import Toast from 'react-native-tiny-toast'

import { Input, FlashMessage, LoadingBase } from '@/components';
import { commonStyles, Colors, Fonts } from '@/commons';
import { login } from '../../modules/auth/action';
import * as authActions from '../../modules/auth/action';
import { Checkbox } from '@/components/checkbox';
import { screen } from '@/utils';
import { getAccount, getPassword } from '@/stores';

class LoginContainer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.username != undefined || this.props.username != null ? this.props.username : '',
            password: this.props.password != undefined || this.props.password != null ? this.props.password : '',
            showpassword: 1,
            onFocusu: false,
            onFocusp: false,
        };
    }

    componentDidMount = async () => {
        const account = await getAccount();
        const password = await getPassword();
        this.setState({
            username: account != undefined || account != null ? account : '',
            password: password != undefined || password != null ? password : '',
        })
        // if (this.state.username != '' && this.state.password != '') {
        //     this.onLogin({ username: this.state.username, password: this.state.password })
        // }
    }

    showPassword = async () => {
        await this.setState({
            showpassword: (this.state.showpassword + 1) % 2
        })
    }

    onCheckLogin = () => {
        const { username, password } = this.state;
        if (username != '' && password != '') {
            return 1;
        }
        return 0;
    }

    onLogin = () => {
        const { username, password } = this.state;
        if (this.onCheckLogin() == 1) {
            this.props.dispatch(login({ username, password }));
        }
        else {
            FlashMessage('Thông báo !!!', 'warning', 'Vui lòng điền đầy đủ !');
        }
    }

    render() {
        const { isLoading } = this.props;

        return (
            <View style={commonStyles.container} >
                <View style={styles.bgLogo}>
                    {/* <Image source={require('../../assets/images/bg.png')} style={styles.imgbg} /> */}
                </View>
                <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                    <View>
                        <View style={[styles.inputlogin, { borderColor: this.state.onFocusu ? Colors.mainColor : Colors.colorUnenable }]}>
                            <Icon name="user" size={16} color={this.state.onFocusu ? Colors.mainColor : Colors.colorUnenable} style={styles.iconlogin} />
                            <Input
                                style={[styles.inputview, { color: this.state.onFocusu ? Colors.mainColor : Colors.colorUnenable }]}
                                placeholder='Tài khoản'
                                placeholderTextColor={this.state.onFocusu ? Colors.mainColor : Colors.colorUnenable}
                                onChangeText={(value) => this.setState({ username: value })}
                                onFocus={async () => { await this.setState({ onFocusu: true }) }}
                                onBlur={async () => { await this.setState({ onFocusu: false }) }}
                                value={this.state.username}
                            />
                        </View>
                        <View style={[styles.inputlogin, { borderColor: this.state.onFocusp ? Colors.mainColor : Colors.colorUnenable }]}>
                            <Icon name="lock" size={16} color={this.state.onFocusp ? Colors.mainColor : Colors.colorUnenable} style={styles.iconlogin} />
                            <Input
                                style={[styles.inputview, { color: this.state.onFocusp ? Colors.mainColor : Colors.colorUnenable }]}
                                placeholder='Mật khẩu'
                                placeholderTextColor={this.state.onFocusp ? Colors.mainColor : Colors.colorUnenable}
                                secureTextEntry={this.state.showpassword == 1 ? true : false}
                                onChangeText={(value) => this.setState({ password: value })}
                                onFocus={async () => { await this.setState({ onFocusp: true }) }}
                                onBlur={async () => { await this.setState({ onFocusp: false }) }}
                                value={this.state.password}
                            />
                            <TouchableOpacity
                                onPress={() => { this.showPassword() }}
                            >
                                <Icon name="tripadvisor" size={16} color={this.state.showpassword == 1 ? Colors.colorUnenable : Colors.mainColor} style={{ marginRight: 20 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center', padding: 10 }}>
                        <TouchableOpacity
                            onPress={() => this.onLogin({ username: this.state.username, password: this.state.password })}

                        >
                            <View style={styles.buttonLogin}>
                                <Text style={styles.textBtLogin}>
                                    Đăng nhập
                                </Text>
                                <LoadingBase visible={isLoading} color={Colors.colorWhite} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
                <View style={commonStyles.footer}>
                    <Text style={commonStyles.textfooter}>Copyright {'\u00A9'} by YourProject </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={commonStyles.textfooter}>Powered by </Text>
                        <Image source={require('../../assets/icons/logoirtech.png')} style={{ width: 60, height: 20 }} />
                    </View>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    buttonLogin: {
        backgroundColor: Colors.mainColor,
        marginTop: 30,
        width: screen.widthsc / 1.5,
        height: 40,
        borderRadius: 3,
        borderBottomColor: Colors.colorBorder,
        borderBottomWidth: 1,
        borderRightColor: Colors.colorBorder,
        borderRightWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        flexDirection: 'row'
    },
    textBtLogin: {
        fontFamily: Fonts.SairaSemiCondensedBold,
        color: Colors.colorTextWhite,
    },
    inputview: {
        position: 'relative',
        width: screen.widthsc / 1.5,
        fontFamily: Fonts.SairaSemiCondensedBold,
    },
    iconlogin: {
        height: 20,
        width: 20,
        marginLeft: 20,
    },
    inputlogin: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        marginBottom: 20,
        borderRadius: 3,
    },
})

const mapStateToProps = (state) => {
    return {
        username: state.loginReducers.toJS().username,
        password: state.loginReducers.toJS().password,
        userID: state.loginReducers.toJS().userID,
        isLoading: state.loginReducers.toJS().isLoading,
    };
};

export default connect(mapStateToProps)(LoginContainer);