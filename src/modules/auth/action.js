import * as Actions from './constants';

export const login = (params, onSuccess, onError) => ({
  type: Actions.LOGIN,
  params,
  onSuccess,
  onError
})

export const setUserInfo = (data) => ({
  type: Actions.SET_USER_INFO,
  data,
})

export const fetchUserInfo = (onSuccess, onError) => ({
  type: Actions.FETCH_USER_INFO,
  onSuccess,
  onError
})

export const home = (params, gohome) => ({
  type: Actions.GO_HOME,
  params,
  gohome,
})


export const signOut = () => ({
  type: Actions.LOGOUT
})