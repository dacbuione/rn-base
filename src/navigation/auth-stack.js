import { createStackNavigator } from 'react-navigation-stack';
import LoginContainer from '../screens/auth/login';

import { authStack } from '../config/navigator';

export default createStackNavigator(
    {
        [authStack.login]: LoginContainer,
    },
    {
        defaultNavigationOptions: {
            headerShown: false
        },
    }
);