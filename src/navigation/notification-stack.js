import { createStackNavigator } from 'react-navigation-stack';

import NotificationScreen from '../screens/notification/notification.screen';

import { notificationStack } from '../config/navigator';

export default createStackNavigator(
    {
        // home: home,
        [notificationStack.notification]: NotificationScreen,

    },
    {
        defaultNavigationOptions: {
            header: null,
        },
    }
);