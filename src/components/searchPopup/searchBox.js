import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import { Colors } from '@/commons';
export default class SeachBox extends Component{
    constructor(props){
        super(props);
        this.state = {
            searchVal:'',
        }
    }

    _handleChange = (searchVal) =>{
       this.setState({
           searchVal:searchVal
       });
    }
    _renderClearButton = ()=>{
        if(this.state.searchVal!==''){  
            return (<TouchableOpacity onPress={()=> { this.setState({searchVal:''}) }}  style={styles.iconClose}><Icon name="close" size={20} color='red'></Icon></TouchableOpacity>)
        }
        return null;
    }
   
    render(){
        var {placeholder, style, left} = this.props;
        return(
            <View style={[styles.searchBox,{left:left== undefined ? 0 : left}]}>
                <TextInput
                    underlineColorAndroid="transparent"
                    placeholder={placeholder}
                    style={[styles.inputSearch, style]}
                    value={this.state.searchVal}
                    onChangeText={(text) => this._handleChange(text)}
                    />
                    <Icon name="search" size={18} style={styles.iconSearch} color={Colors.mainColor}></Icon>
                    {this._renderClearButton()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    searchBox:{
        backgroundColor:Colors.mainColor,
        borderRadius:20,
        borderWidth:0,
        padding:1.5,
        margin:10,
        justifyContent:'center'
    },
    inputSearch:{
        backgroundColor:'#fff',
        borderRadius:20,
        paddingVertical:5,
        paddingHorizontal:40
    },
    iconSearch:{
        position:'absolute',
        left:15,    
        marginRight:5,
    },
    iconClose:{
        position:'absolute',
        right:15,
    }
});