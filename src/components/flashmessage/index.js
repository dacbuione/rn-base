import FlashMessage from './flashmessage';
import ErrorMessage from './errormessage';

export {
    FlashMessage,
    ErrorMessage
}