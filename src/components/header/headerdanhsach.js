import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Dimensions } from 'react-native';
import { Colors } from '@/commons';
import { Icons } from '@/components'
import { NavigationActions, StackActions } from 'react-navigation';
import { muahangStack } from '../../config/navigator';
class HeaderList extends Component {

    gohome = () => {
        const { navigation } = this.props.navigation;
        const resetAction = navigation.navigate(muahangStack.menu);
        navigation.dispatch(resetAction);
    }
    goBack = () => {
        const { navigation } = this.props.navigation;
        navigation.pop()
        
    }
    render() {
        const { filter , title } = this.props;
        const color = Colors.mainColor;
        return (
            <View style={styles.container} >
                <TouchableOpacity style={styles.left} onPress={()=> this.goBack()}>
                    <Icons name='left-arrow' color={color} size={24} />
                </TouchableOpacity>
                <Text style={styles.center}>{title}</Text>
                <View  style={styles.right}>
                    <TouchableOpacity  style={{margin:10, marginRight:0}} onPress={this.gohome}>
                        <Icons name='menu' color={color} size={24}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        height:50,
        borderBottomColor:'#ddd',
        borderBottomWidth:1,
        width:Dimensions.get('screen').width
    },
    left: {
        flex:1,
        margin:10,
        marginLeft:20,
    },
    center: {
        flex:6,
        left:-20,
        fontWeight:'bold',
        textAlign:'center',
        fontSize:17,
        color:Colors.mainColor
    },
    right: {
        flexDirection:'row',
        flex:1,
        justifyContent:'flex-end',
        right:10
    },
    filter:{
        width:30,
        height:30,
        borderWidth:1,
        borderColor:Colors.mainColor,
        borderRadius:30,
        justifyContent:'center',
        alignItems:'center',
        top:7,
        right:2,
    }
})

export default HeaderList;