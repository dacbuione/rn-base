import React from 'react';

import { createBottomTabNavigator } from 'react-navigation-tabs';

import home from '../screens/home/home';
import NotificationStack from '../navigation/notification-stack';
import SearchStack from '../navigation/search-stack';
import ProfileStack from '../navigation/profile-stack';

import { homeTab } from '../config/navigator';

import Icons from 'react-native-vector-icons/FontAwesome';

NotificationStack.navigationOptions = {
  tabBarLabel: 'Thông báo',
  tabBarIcon: ({ focused }) => {
    return (
      <Icons name='bell-o' size={focused ? 30 : 18} color={focused ? '#7bbcfb' : '#BBBBBB'} />
    )
  }
}

SearchStack.navigationOptions = {
  tabBarLabel: 'Tìm kiếm',
  tabBarIcon: ({ focused }) => {
    return (
      <Icons name='search' size={focused ? 30 : 18} color={focused ? '#7bbcfb' : '#BBBBBB'} />
    )
  }
}

home.navigationOptions = {
  tabBarLabel: 'Trang chủ',
  tabBarIcon: ({ focused }) => {
    return (
      <Icons name='home' size={focused ? 30 : 18} color={focused ? '#7bbcfb' : '#BBBBBB'} />
    )
  }
}

ProfileStack.navigationOptions = {
  tabBarLabel: 'Hồ sơ',
  tabBarIcon: ({ focused }) => {
    return (
      <Icons name='user' size={focused ? 30 : 18} color={focused ? '#7bbcfb' : '#BBBBBB'} />
    )
  }
}

const HomeTabs = createBottomTabNavigator(
  {
    [homeTab.home]: {
      screen: home,
    },
    [homeTab.search]: {
      screen: SearchStack,
    },
    [homeTab.notification]: {
      screen: NotificationStack,
    },
    [homeTab.profile]: {
      screen: ProfileStack,
    },
  },
  {
    initialRouteName: homeTab.home,
    // tabBarOptions: {
    //     style: {
    //         backgroundColor: '#fff'
    //     },
        activeTintColor: '#7bbcfb',
        inactiveTintColor: '#7bbcfb',
    // }
  },
);

export default HomeTabs;