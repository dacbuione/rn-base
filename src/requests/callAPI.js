import base64 from 'react-native-base64';
import md5 from 'md5';

import { privateKey, URL } from '../services/config';
import { CheckInternetFetch } from '@/utils';

export default loginAPI = (params) => {
  if (CheckInternetFetch()) {
    var data = base64.encode(JSON.stringify(params));
    var pk = privateKey + data;
    var md5Key = md5(pk);
    var arrKey = Array.from(md5Key);
    var Key =
      arrKey[12] +
      arrKey[3] +
      arrKey[11] +
      arrKey[13] +
      arrKey[12] +
      arrKey[20] +
      arrKey[19] +
      arrKey[15] +
      arrKey[8] +
      arrKey[6];
    var nameFuntion = base64.encode('');
    var formdata = new FormData();
    formdata.append("form", nameFuntion);
    formdata.append("data", data);
    formdata.append("key", Key);
    console.log('param', params)
    var requestOptions = {
      method: 'POST',
      body: formdata,
    };
    return fetch(URL, requestOptions)
      .then(response => response.json())
      .then(result => result)
      .catch(error => error);
  }
}

