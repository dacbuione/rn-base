
export const authStack = {
    login: 'LoginScreen',
};

export const splashStack = {
    splash: 'SplashScreen',
};

export const muahangStack = {

    menu: 'menuScreen',

    listDemandForSupplies: 'listDemandForSuppliesScreen', //danh sách phiếu nhu cầu vật tư
    newDemandForSupplies: 'tncvtScreen', //thêm phiếu nhu cầu vật tư
    newDetailDemandForSupplies: 'tctpncvtScreen', // thêm chi tiết phiếu nhu cầu vật tư
    editDetailDemandForSupplies: 'suactpncvt', //Sửa chi tiết phiếu nhu cầu vật tư
    editDemandForSupplies: 'suancvtStack', // sửa phiếu nhu cầu vật tư

    listDomesticOrders: 'dsdhmtnScreen', //ds đơn hàng mua trong nước
    detailDomesticOrders: 'xctdhmtnScreen', // xem chi tiết đơn hàng mua trog nước
    newDomesticOrders: 'tdhmtnScreen', // thêm đơn hàng mua trong nước
    editDomesticOrders: 'sdhmtnScreen', // sửa đơn hàng mua trong nước

    listImportOrders: 'dsdhnkScreen', // ds đơn hàng nhập khẩu
    detailImportOrders: 'xctdhnkScreen', // Xem chi tiết đơn hàng nhập khẩu
    newImportOrders: 'tdhnkScreen', // thêm đơn hàng nhập khẩu
    editImportOrders: 'sdhnkScreen', // sửa đơn hàng nhập khẩu

    orderApproval: 'dsdtdhScreen', // danh sách duyệt đơn hàng

    closeOder: 'dsdgdhScreen', // danh sách đóng đơn hàng
    
    dsncvtcd: 'dsncvtcdScreen', // danh sách phiếu nhu cầu cần duyệt
    listDemandForSlips: 'dsncvtddScreen', // danh sách phiếu nhu cầu đã duyệt
    detailDemandSlips: 'ctncvtcdScreen', // chi tiet phiếu nhu cầu duyệt

    listOfPurchases: 'dspnmScreen', //danh sách phiếu nhập mua
    listOfPurchases: 'danhsachphieunm',
    newOfPurchases: 'themphieunm',
    newDetailOfPurchases: 'themchitietphieunm',
    editOfPurchases: 'suaphieunm',
    editDetailOfPurchases: 'suactphieunm',
    detailOfPurchases: 'chitietphieunm',

    modalview: 'modalScreen',

    //     themncvt: 'themncvtStack',
    //     newDetailDemandForSupplies: 'newDetailDemandForSupplies',
    //     suactpncvt: 'suactpncvt',
    //     suancvt: 'suancvtStack',
    //     dscd: 'dscd'

}

export const notificationStack = {
    notification: 'NoticationScreen',
}

export const searchStack = {
    search: 'SearchScreen',
}

export const profileStack = {
    profile: 'ProfileScreen',
}

export const homeStack = {
    home: 'HomeTab'
}

export const homeTab = {
    home: 'HomeScreen',
    search: 'SearchScreen',
    notification: 'NotificationScreen',
    profile: 'ProfileScreen',
}

export const rootSwitch = {
    // splash: 'SplashStack',
    auth: 'AuthStack',
    main: 'MainStack',

};
export const mainStack = {
    home: 'HomeStack',
    muahang: 'muahangStack',
    profile: 'ProfileStack',
    search: 'SearchStack',
    notification: 'NotificationStack',
};