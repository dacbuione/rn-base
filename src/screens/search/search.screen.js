import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';

class SearchScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>SearchScreen</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    }
})

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, null)(SearchScreen)