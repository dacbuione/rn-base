import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64';



const FCM_TOKEN = 'fcm_token';
const KEY_PROFILE = 'profile';
const KEY_TOKEN = 'access_token';

const KEY_ACCOUNT = 'account';
const KEY_PASSWORD = 'password';
const KEY_USERID = 'id'

export const saveAccount = (account) => {
    AsyncStorage.setItem(KEY_ACCOUNT, account);
};

export const savePassword = (password) => {
    AsyncStorage.setItem(KEY_PASSWORD, password)
}

export const getAccount = () => {
    return AsyncStorage.getItem(KEY_ACCOUNT);
};

export const getPassword = () => {
    return AsyncStorage.getItem(KEY_PASSWORD);
}

export const saveUserID = (id) => {
    AsyncStorage.setItem(KEY_USERID, id.toString());
}


export const getUserID = () => {
    return AsyncStorage.getItem(KEY_USERID);
};

export const logOut = () => {
    AsyncStorage.clear()
}; 