import { createStackNavigator } from 'react-navigation-stack';

import ProfileScreen from '../screens/profile/profile.screen';

import { profileStack } from '../config/navigator';

export default createStackNavigator(
    {
        // home: home,
        [profileStack.profile]: ProfileScreen,

    },
    {
        defaultNavigationOptions: {
            header: null,
        },
    }
);