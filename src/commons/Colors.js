const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  greyColor: '#999',
  successColor: '#4caf50',
  pinkColor: '#ff4081',

  mainColor:'#00bcff',
  lightMainColor:'#ebf3f5',

  bgLoginScreen: '#8CE0FF',
  bgContainerColor: '#FFF',

  colorUnenable: '#c7c7c7',
  colorBorder: '#BBBBBB',
  colorTextWhite: '#FFF',
  colorWhite: '#FFF',
};

const percentToHex = (p) => {
  const intValue = ~~(p / 100 * 255); // map percent to nearest integer (0 - 255)
  const hexValue = intValue.toString(16); // get hexadecimal representation
  return hexValue.padStart(2, '0').toUpperCase(); // format with leading 0 and upper case characters
}

export const colorOpacityMaker = (color, opacity) => `${color}${percentToHex(opacity)}`

