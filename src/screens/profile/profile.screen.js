import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import screen from '../../utils/screen';

import { signOut } from '../../modules/auth/action';

class ProfileScreen extends Component {

    logout = () => {
        this.props.dispatch(signOut())
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.onLogout}
                    onPress={() => { this.logout() }}
                >
                    <Image source={require('../../assets/icons/ic_logout.png')} style={styles.iconLogout} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    header: {
        position: 'relative',
        width: screen.widthsc,
        // backgroundColor: 'black',
        height: screen.heightsc / 16,
        alignItems: 'center',
        // flexDirection: 'row'
    },
    body: {
        width: screen.widthsc,
        backgroundColor: '#7bbcfb',
        height: (screen.heightsc - (screen.heightsc / 16)) / 4,
        // position: 'absolute',
        alignItems: 'center',
    },
    titleHeader: {
        color: '#BBBBBB',
        fontSize: 20,
        marginTop: 10,
    },
    borderItem1: {
        backgroundColor: '#FFF',
        width: screen.widthsc - 40,
        height: screen.heightsc / 5,
        marginTop: screen.widthsc / 2.8,
        margin: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 5,
        flexDirection: 'row',
    },
    borderItem2: {
        backgroundColor: '#FFF',
        width: screen.widthsc - 40,
        height: screen.heightsc / 3,
        marginTop: 1,
        margin: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 5,
        flexDirection: 'column',
    },
    iconUser: {
        margin: screen.widthsc / 10,
    },
    imgUser: {
        width: screen.widthsc / 5,
        height: screen.widthsc / 5,
    },
    nameUser: {
        marginTop: screen.widthsc / 7,
        flexDirection: 'column',
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    des: {
        fontSize: 16,
        // fontWeight: 'bold',
        color: '#BBBBBB'
    },
    iconLogout: {
        width: screen.widthsc / 15,
        height: screen.widthsc / 15,
        margin: 10,
    },
    onLogout: {
        position: 'absolute',
        right: 0,
    }
})

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, null)(ProfileScreen)