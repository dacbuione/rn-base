import { put, call, all, fork, takeLatest, takeEvery } from 'redux-saga/effects';
import * as types from './constants';
import { mainStack } from '../../config/navigator';
import NavigationService from '../../utils/navigation';
// import { saveAccount, getAccount } from '../../stores/stores';


function* goMuahang() {
    yield call(NavigationService.navigate, mainStack.muahang);
}

export default function* homeSaga() {
    yield takeEvery(types.MUA_HANG, goMuahang)
}