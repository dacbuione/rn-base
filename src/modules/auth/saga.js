import { put, call, all, fork, takeLatest, takeEvery } from 'redux-saga/effects';
import * as types from './constants';
import {
    loginAuth,
    logOutAuth
} from './service';

import { rootSwitch, mainStack } from '../../config/navigator';
import NavigationService from '../../utils/navigation';
import { saveAccount, savePassword, saveUserID, getAccount } from '@/stores';
import { ErrorMessage } from '@/components';

function* login(action) {
    try {
        yield put({
            type: types.LOGIN_LOADING,
            isLoading: true,
        });
        const response = yield call(loginAuth, action.params); //Gọi API Login ở đây.
        yield call(doLoginSuccess, response, action.params);
    } catch (err) {
        ErrorMessage(err.toString());
        yield put({
            type: types.LOGIN_LOADING,
            isLoading: false,
        });
    }
}

function* doLoginSuccess(result, account) {
    try {
        if (typeof (result) == 'object')
            if (result.loai == 1) {
                saveAccount(account.username);
                savePassword(account.password);
                saveUserID(result.userId);
                yield call(NavigationService.navigate, mainStack.home);
                yield put({
                    type: types.LOGIN_SUCCESS,
                    msg: 'Đăng nhập thành công !',
                    type_msg: 'success',
                    userId: result.userId,
                    isLoading: false,
                });
            }
            else {
                yield put({
                    type: types.LOGIN_FAIL,
                    msg: 'Đăng nhập thất bại !',
                    type_msg: 'danger',
                    isLoading: false,
                });
            }
        else if (Array.isArray()) {
            ErrorMessage('Server trả về không đúng kiểu dữ liệu !');
            yield put({
                type: types.LOGIN_LOADING,
                isLoading: false,
            });
        }
        else {
            ErrorMessage('Mất kết nối đường truyền ... !');
            yield put({
                type: types.LOGIN_LOADING,
                isLoading: false,
            });
        }
    } catch (err) {
        ErrorMessage(err.toString());
        yield put({
            type: types.LOGIN_LOADING,
            isLoading: false,
        });
    }
}


function* logOut() {
    try {
        yield call(logOutAuth);
        yield call(NavigationService.navigate, rootSwitch.auth);
    } catch (err) {
        ErrorMessage(err.toString());
    }
}

export default function* authSaga() {
    yield takeEvery(types.LOGIN, login);
    yield takeEvery(types.LOGOUT, logOut);
}