import { fromJS } from 'immutable';

import * as types from './constants';
// import appState from '../contants/initialState';
import { getAccount, getPassword, getUserID } from '@/stores';
import { FlashMessage } from '@/components';

const login = fromJS({
  username: '',
  password: '',
  userID: '',
  message: '',
  type_message: '',
  isLoading: false,
})

export default function loginReducers(state = login, action) {

  switch (action.type) {

    case types.LOGIN:
      console.log('types.LOGIN')
      return state;

    case types.LOGIN_LOADING:
      console.log('types.LOGIN_LOADING')
      return state
        .set('isLoading', action.isLoading);

    case types.LOGIN_SUCCESS:
      console.log('types.LOGIN_SUCCESS')
      FlashMessage(action.msg, action.type_msg)
      return state
        .set('userID', action.userID)
        .set('isLoading', action.isLoading);

    case types.LOGIN_FAIL:
      console.log('types.LOGIN_FAIL')
      FlashMessage(action.msg, action.type_msg)
      return state
        .set('isLoading', action.isLoading)

    case types.LOGOUT:
      console.log('types.LOGOUT')
      FlashMessage('Đăng xuất thành công !', 'success')
      return state
        .set('username', '')
        .set('password', '')
        .set('userID', '')
        .set('message', '')
        .set('type_message', '')
        .set('isLoading', false)

    default:
      console.log('default')
      return state;
  }
}
